package sdk.models;

/**
 * Created by cibehariharan on 28/06/19.
 */
public class Progression {
    int totalAttempts, successfulAttempts;
    Attempt [] attempts;
    public Progression(int totalAttempts, int successfulAttempts, Attempt [] attempts) {
        this.totalAttempts = totalAttempts;
        this.successfulAttempts = successfulAttempts;
        this.attempts = attempts;
    }

    public int getSuccessfulAttempts() {
        return successfulAttempts;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }

    public Attempt[] getAttempts() {
        return attempts;
    }
}
