package sdk.models;

/**
 * Created by cibehariharan on 18/04/19.
 */
/**
 * Created by cibehariharan on 18/04/19.
 */
public enum ResourceType
{
    QUESTION, VIDEO;

    public static ResourceType valueOf(int x) {
        switch(x) {
            case 0:
                return QUESTION;
            case 1:
                return VIDEO;
        }
        return null;
    }
}
