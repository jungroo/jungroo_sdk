package sdk.models;

public class LearningResource {
    String resourceId, learningObjectiveId;

    ResourceType resourceType;

    int nextSequenceId;

    Boolean isTerminated;

    public int getNextSequenceId() { return nextSequenceId; }

    public String getResourceId() {
        return resourceId;
    }

    public String getLearningObjectiveId() {
        return learningObjectiveId;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public Boolean hasTerminated() {return isTerminated;}

    public LearningResource(String learningObjectiveId,
                            ResourceType resourceType,
                            String resourceId,
                            int nextSequenceId,
                            Boolean isTerminated) {
        this.learningObjectiveId = learningObjectiveId;
        this.resourceType = resourceType;
        this.resourceId = resourceId;
        this.nextSequenceId = nextSequenceId;
        this.isTerminated = isTerminated;
    }
}
