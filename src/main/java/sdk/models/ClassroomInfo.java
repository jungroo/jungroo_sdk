package sdk.models;

/**
 * Created by cibehariharan on 27/06/19.
 */
public class ClassroomInfo {
    String classroomId, schoolId, division;
    int grade;
    String[] studentIds;

    public String getDivision() {
        return division;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public int getGrade() {
        return grade;
    }

    public String [] getStudentIds() {
        return studentIds;
    }

    public ClassroomInfo(String classroomId, String schoolId,
                         int grade, String division, String [] studentIds) {
        this.classroomId = classroomId;
        this.schoolId = schoolId;
        this.division = division;
        this.grade = grade;
        this.studentIds = studentIds;
    }
}
