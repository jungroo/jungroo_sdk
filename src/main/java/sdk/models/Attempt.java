package sdk.models;

/**
 * Created by cibehariharan on 28/06/19.
 */
public class Attempt {
    String [] learningContentIds;
    Double totalScore;
    String attemptedOn;
    public Attempt(String [] learningContentIds,
                   Double totalScore,
                   String attemptedOn) {
        this.learningContentIds = learningContentIds;
        this.totalScore = totalScore;
        this.attemptedOn = attemptedOn;
    }

    public Double getTotalScore() {
        return totalScore;
    }

    public String getAttemptedOn() {
        return attemptedOn;
    }

    public String[] getLearningContentIds() {
        return learningContentIds;
    }
}
