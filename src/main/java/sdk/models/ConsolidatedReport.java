package sdk.models;
import java.util.*;

/**
 * Created by cibehariharan on 28/06/19.
 */
public class ConsolidatedReport {
    String studentId, contextId;
    int reportType, context, learningContentType, scope;
    HashMap<String, Integer> strengths, weaknesses, classAverage, classHigh, classLow, myScore;
    Progression progression;
    public ConsolidatedReport(String studentId, String contextId,
                              int reportType, int context, int learningContentType,
                              int scope,
                              HashMap<String, Integer> strengths,
                              HashMap<String, Integer> weaknesses,
                              HashMap<String, Integer> classAverage,
                              HashMap<String, Integer> classHigh,
                              HashMap<String, Integer> classLow,
                              HashMap<String, Integer> myScore,
                              Progression progression) {
        this.studentId = studentId;
        this.contextId = contextId;
        this.reportType = reportType;
        this.context = context;
        this.learningContentType = learningContentType;
        this.scope = scope;
        this.strengths = strengths;
        this.weaknesses = weaknesses;
        this.classAverage = classAverage;
        this.classHigh = classHigh;
        this.classLow = classLow;
        this.myScore = myScore;
        this.progression = progression;
    }

    public HashMap<String, Integer> getClassAverage() {
        return classAverage;
    }

    public HashMap<String, Integer> getClassLow() {
        return classLow;
    }

    public HashMap<String, Integer> getClassHigh() {
        return classHigh;
    }

    public HashMap<String, Integer> getStrengths() {
        return strengths;
    }

    public HashMap<String, Integer> getWeaknesses() {
        return weaknesses;
    }

    public int getContext() {
        return context;
    }

    public int getLearningContentType() {
        return learningContentType;
    }

    public int getReportType() {
        return reportType;
    }

    public Progression getProgression() {
        return progression;
    }

    public String getContextId() {
        return contextId;
    }

    public String getStudentId() {
        return studentId;
    }

    public int getScope() {
        return scope;
    }

    public HashMap<String, Integer> getMyScore() { return myScore; }
}
