package sdk.models;

/**
 * Created by cibehariharan on 19/04/19.
 */
public class StudentLearningSessionResponse {
    Long timeTaken;
    int sequenceId;
    Double responsePercentage;

    public StudentLearningSessionResponse(int sequenceId, Long timeTaken, Double responsePercentage) {
        this.sequenceId = sequenceId;
        this.responsePercentage = responsePercentage;
        this.timeTaken = timeTaken;
    }

    public Long getTimeTaken() {
        return timeTaken;
    }

    public int getSequenceId() {
        return sequenceId;
    }

    public Double getResponsePercentage() {
        return responsePercentage;
    }
}