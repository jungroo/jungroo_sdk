package sdk.models;

/**
 * Created by cibehariharan on 12/04/19.
 */
public class AssessmentQuestion {
    String questionId, learningObjectiveId;
    Boolean isTerminated;

    public AssessmentQuestion(String learningObjectiveId, String questionId, Boolean isTerminated) {
        this.questionId = questionId;
        this.learningObjectiveId = learningObjectiveId;
        this.isTerminated = isTerminated;
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getLearningObjectiveId() {
        return learningObjectiveId;
    }

    public Boolean hasTerminated() {return isTerminated;}
}
