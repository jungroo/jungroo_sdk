package sdk.models;

/**
 * Created by cibehariharan on 17/04/19.
 */
public class StudentAssessmentResponse {
    Long timeTaken;
    int questionSequenceId;
    Double mark;

    public StudentAssessmentResponse(int questionSequenceId, Double mark, Long timeTaken) {
        this.questionSequenceId = questionSequenceId;
        this.mark = mark;
        this.timeTaken = timeTaken;
    }

    public Long getTimeTaken() {
        return timeTaken;
    }

    public int getQuestionSequenceId() {
        return questionSequenceId;
    }

    public Double getMark() {
        return mark;
    }
}
