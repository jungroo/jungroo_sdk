package sdk.models;


/**
 * Created by cibehariharan on 15/04/19.
 */
public class StudentBook {
    String studentId, bookId;

    public StudentBook(String studentId, String bookId) {
        this.studentId = studentId;
        this.bookId = bookId;
    }

    public String getBookId() {
        return bookId;
    }

    public String getStudentId() {
        return studentId;
    }
}
