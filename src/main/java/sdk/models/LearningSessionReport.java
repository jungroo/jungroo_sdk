package sdk.models;
import javax.json.JsonArray;
import java.util.*;

/**
 * Created by cibehariharan on 20/05/19.
 */
public class LearningSessionReport {
    HashSet<String> conceptsUnderstood = new HashSet<String>();
    HashSet<String> conceptsNotUnderstood = new HashSet<String>();

    public LearningSessionReport(JsonArray conceptsUnderstoodArray,
                                 JsonArray conceptsNotUnderstoodArray) {
        for (int index=0; index < conceptsUnderstoodArray.size(); index++) {
            this.conceptsUnderstood.add(conceptsUnderstoodArray.getString(index));
        }
        for (int index=0; index < conceptsNotUnderstoodArray.size(); index++) {
            this.conceptsNotUnderstood.add(conceptsNotUnderstoodArray.getString(index));
        }
    }

    public void print() {
        System.out.println("------------------------------");
        System.out.println("Concepts understood by student");
        System.out.println("------------------------------");
        Iterator<String> it = conceptsUnderstood.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        System.out.println("----------------------------------");
        System.out.println("Concepts not understood by student");
        System.out.println("----------------------------------");
        it = conceptsNotUnderstood.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
