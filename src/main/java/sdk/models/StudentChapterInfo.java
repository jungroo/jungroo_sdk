package sdk.models;

/**
 * Created by cibehariharan on 16/04/19.
 */
public class StudentChapterInfo {
    String chapterId, studentId;

    public StudentChapterInfo(String studentId, String chapterId) {
        this.chapterId = chapterId;
        this.studentId = studentId;
    }

    public String getChapterId() {
        return chapterId;
    }

    public String getStudentId() {
        return studentId;
    }
}
