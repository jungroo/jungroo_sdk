package sdk.models;

/**
 * Created by cibehariharan on 28/06/19.
 */
public class ReportRequestParams {
    int reportType, context, learningContentType, scope;
    String contextId;
    public ReportRequestParams(int reportType,
                        int context,
                        int learningContentType,
                        int scope,
                        String contextId) {
        this.reportType = reportType;
        this.context = context;
        this.learningContentType = learningContentType;
        this.scope = scope;
        this.contextId = contextId;
    }

    public String getContextId() {
        return contextId;
    }

    public int getReportType() {
        return reportType;
    }

    public int getLearningContentType() {
        return learningContentType;
    }

    public int getScope() {
        return scope;
    }

    public int getContext() {
        return context;
    }
}
