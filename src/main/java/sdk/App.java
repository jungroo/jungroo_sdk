package sdk;
import sdk.apis.*;
import sdk.models.*;

import java.util.Scanner;
import java.io.IOException;

/**
 * Created by cibehariharan on 16/04/19.
 */
public class App {

    public static void main(String args[]) throws IOException {
        //testTceAPI();
        //testCrmAPI();
        testConsolidatedReportAPI();
    }

    public static void testSessionAPIs() throws IOException {
        JungrooSessionApi jungrooSessionApi = new JungrooSessionApi();
        jungrooSessionApi.initializeBookForStudent("s4", "d1edb57b-bcd9-4b87-948e-fdf60df9ce1f");
    }

    public static void testContentAPIs() throws IOException {
        JungrooContentApi jungrooContentApi = new JungrooContentApi();
        jungrooContentApi.uploadTeachingPointInfo("tce-contents/Teaching_points.csv");
        jungrooContentApi.uploadMappingFile("tce-contents/Updated_Lo_Tp_map.csv");
        jungrooContentApi.uploadResourceInfo("tce-contents/Resources.csv");
        jungrooContentApi.uploadQuestionResources("tce-contents/Question-TP-LOIDs.csv");
        jungrooContentApi.uploadConceptDependencies("tce-contents/Dependencies.csv");
        jungrooContentApi.uploadBookXMLFile("tce-contents/custom-book.xml");
    }

    public static void testLearningAPIs() throws IOException {
        JungrooLearningApi jungrooLearningApi = new JungrooLearningApi();
        Scanner input = new Scanner(System.in);
        Long studentLearningSessionId = jungrooLearningApi.initializeLearningSession("s7", "chp-d7a30a57-b359-469d-85e8-4090426b809c");
        if(studentLearningSessionId != -1) {
            LearningResource learningResource = jungrooLearningApi.startLearningSession(studentLearningSessionId);
            if (learningResource.getResourceId().equals("-1")) {
                System.out.println("Existing resources are showed already, Cannot proceed the learning session");
            } else {
                while (!learningResource.hasTerminated()) {
                    System.out.println(learningResource.getLearningObjectiveId() +
                            " " + learningResource.getResourceType() +
                            " " + learningResource.getResourceId());
                    // marks for the question and timetaken for the question are hard-coded ad 0.0 and 12 seconds
                    if(learningResource.getResourceType() == ResourceType.QUESTION) {
                        System.out.println("Enter the marks scored for this question");
                        Double mark = input.nextDouble();
                        System.out.println("Enter the timetaken for this question");
                        Long timeTaken = input.nextLong();
                        learningResource = jungrooLearningApi.continueLearningSession(studentLearningSessionId, learningResource, timeTaken, mark);
                    } else {
                        System.out.println("Enter the percentage video viewed [0-100]");
                        Double percentageViewed = (input.nextDouble()/100);
                        System.out.println("Enter the timetaken for this video");
                        Long timeTaken = input.nextLong();
                        learningResource = jungrooLearningApi.continueLearningSession(studentLearningSessionId, learningResource, timeTaken, percentageViewed);
                    }
                    if (learningResource.getResourceId().equals("-1")) {
                        System.out.println("Existing resources are showed already for " + learningResource.getLearningObjectiveId() + ", Cannot proceed the learning session");
                        break;
                    }
                }
                if(learningResource.hasTerminated()) {
                    testReportingAPIs(studentLearningSessionId);
                }
            }
        } else {
            System.out.println("Cannot initialize the learning session");
        }
    }

    public static void testTceAPI() throws IOException {
        JungrooLearningApi jungrooLearningApi = new JungrooLearningApi();
        LearningResource learningResource = new LearningResource("1", ResourceType.QUESTION, "R1", 10, false);
        LearningResource learningResource1 = jungrooLearningApi.continueLearningSession(172L, learningResource, 12L, 0.0);
        System.out.println(learningResource1.hasTerminated());
    }

    public static void testCrmAPI() throws IOException {
        JungrooCrmApi jungrooCrmApi = new JungrooCrmApi();
        String [] studentIds = {"stu-04", "stu-05", "stu-06"};
        jungrooCrmApi.createClassroom("cl-01", "sch-01", 5, "5B", studentIds);
        ClassroomInfo classroomInfo = jungrooCrmApi.getClassroomDetails("cl-01");
    }

    public static void testConsolidatedReportAPI() throws IOException {
        ReportRequestParams reportRequestParams = new ReportRequestParams(1, 1, 1, 1, "chp-01");
        JungrooReportsApi jungrooReportsApi = new JungrooReportsApi();
        jungrooReportsApi.getConsolidatedReportsForStudent("stu-1", reportRequestParams);
    }

    public static void testAssessmentAPIs() throws IOException {
        JungrooAssessmentApi jungrooAssessmentApi = new JungrooAssessmentApi();
        Long studentAssessmentId = jungrooAssessmentApi.initializeAssessment("stud-2", "chp-1");
        if(studentAssessmentId != -1) {
            AssessmentQuestion assessmentQuestion = jungrooAssessmentApi.startAssessment(studentAssessmentId);
            if(assessmentQuestion.getQuestionId().equals("-1")) {
                System.out.println("Existing questions are asked already for " + assessmentQuestion.getLearningObjectiveId() + ". Cannot proceed the assessment");
            } else {
                int sequenceId = 1;
                while (!assessmentQuestion.hasTerminated()) {
                    System.out.println(assessmentQuestion.getLearningObjectiveId() + " " + assessmentQuestion.getQuestionId());
                    // marks for the question and timetaken for the question are hard-coded ad 1.0 and 24 seconds
                    assessmentQuestion = jungrooAssessmentApi.continueAssessment(studentAssessmentId, sequenceId, 1.0, 24L);
                    sequenceId = sequenceId + 1;
                    if (assessmentQuestion.getQuestionId().equals("-1")) {
                        System.out.println("Existing questions are asked already, Cannot proceed the assessment");
                        break;
                    }
                }
            }
        } else {
            System.out.println("Cannot initialize the assessment");
        }
    }

    public static void testReportingAPIs(Long studentLearningSessionId) throws IOException {
        JungrooReportsApi jungrooReportsApi = new JungrooReportsApi();
        LearningSessionReport learningSessionReport = jungrooReportsApi.getReportsForLearningSession(studentLearningSessionId);
        learningSessionReport.print();
    }
}
