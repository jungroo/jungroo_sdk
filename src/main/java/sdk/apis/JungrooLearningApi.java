package sdk.apis;

import sdk.models.LearningResource;
import sdk.models.*;

import javax.json.JsonObject;
import java.io.IOException;

/**
 * Created by cibehariharan on 18/04/19.
 */
public class JungrooLearningApi extends JungrooApi {
    public JungrooLearningApi() {
        super("https://jungroo.com/");
    }

    public Long initializeLearningSession(String studentId, String chapterId) throws IOException {
        StudentChapterInfo studentChapterInfo = new StudentChapterInfo(studentId, chapterId);
        JsonObject jsonObject = apiClient.postApi(hostEndPoint + "tce/initialize_learning_session", getJsonString(studentChapterInfo));
        return jsonObject.getJsonNumber("data").longValueExact();
    }

    public LearningResource startLearningSession(Long studentLearningSessionId) throws IOException {
        JsonObject jsonObject = apiClient.postApi(hostEndPoint + "tce/start_learning_session/" + studentLearningSessionId, EMPTY_JSON);
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        LearningResource learningResource =
                new LearningResource(dataJsonObject.getString("learningObjectiveId"),
                                     ResourceType.valueOf(dataJsonObject.getInt("resourceType")),
                                     dataJsonObject.getString("resourceId"),
                                     dataJsonObject.getInt("nextSequenceId"),
                                     dataJsonObject.getBoolean("isTerminated"));
        return learningResource;
    }

    public LearningResource continueLearningSession(Long studentLearningSessionId, LearningResource learningResource, Long timeTaken, Double responsePercentage) throws IOException {
        StudentLearningSessionResponse studentResponse =
                new StudentLearningSessionResponse(learningResource.getNextSequenceId(), timeTaken, responsePercentage);
        JsonObject jsonObject;
        if(learningResource.getResourceType() == ResourceType.QUESTION) {
            jsonObject = apiClient.postApi(hostEndPoint + "tce/continue_learning_session_after_question/" + studentLearningSessionId, getJsonString(studentResponse));
        } else {
            jsonObject = apiClient.postApi(hostEndPoint + "tce/continue_learning_session_after_video/" + studentLearningSessionId, getJsonString(studentResponse));
        }
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        LearningResource nextLearningResource =
                new LearningResource(dataJsonObject.getString("learningObjectiveId"),
                        ResourceType.valueOf(dataJsonObject.getInt("resourceType")),
                        dataJsonObject.getString("resourceId"),
                        dataJsonObject.getInt("nextSequenceId"),
                        dataJsonObject.getBoolean("isTerminated"));
        return nextLearningResource;
    }
}
