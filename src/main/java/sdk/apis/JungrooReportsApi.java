package sdk.apis;
import sdk.models.*;

import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonArray;
import javax.json.JsonValue;
import java.io.IOException;
import java.util.*;

/**
 * Created by cibehariharan on 20/05/19.
 */
public class JungrooReportsApi extends JungrooApi {
    public JungrooReportsApi() { super("https://jungroo.com/");}

    public LearningSessionReport getReportsForLearningSession(Long learningSessionId) throws IOException {
        JsonObject jsonObject = apiClient.getApi(hostEndPoint + "tce/learning_session_reports/" + learningSessionId);
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        LearningSessionReport learningSessionReport = new LearningSessionReport(dataJsonObject.getJsonArray("conceptsUnderstood"), dataJsonObject.getJsonArray("conceptsNotUnderstood"));
        return learningSessionReport;
    }

    public ConsolidatedReport getConsolidatedReportsForStudent(String studentId,
                                                               ReportRequestParams reportRequestParams) throws IOException {
        JsonObject jsonObject = apiClient.postApi(hostEndPoint + "tce/reports/" + studentId, getJsonString(reportRequestParams));
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        JsonObject progressionJsonObject = dataJsonObject.getJsonObject("myProgression");
        Progression progression = getProgressionFromProgressJson(progressionJsonObject);
        ConsolidatedReport consolidatedReport = getConsolidatedReportFromDataJson(dataJsonObject, progression);
        return consolidatedReport;
    }

    private Progression getProgressionFromProgressJson(JsonObject progressionJsonObject) {
        JsonArray attemptsJsonArray = progressionJsonObject.getJsonArray("attempts");
        Attempt [] attempts = new Attempt[attemptsJsonArray.size()];
        for (int attemptIndex = 0; attemptIndex < attemptsJsonArray.size(); attemptIndex++) {
            JsonObject attemptJsonObject = attemptsJsonArray.getJsonObject(attemptIndex);
            JsonArray learningContentArray = attemptJsonObject.getJsonArray("learningContentIds");
            String [] learningContentIds = new String[learningContentArray.size()];
            for (int lcIndex = 0; lcIndex < learningContentArray.size(); lcIndex++) {
                learningContentIds[lcIndex] = learningContentArray.getString(lcIndex);
            }
            attempts[attemptIndex] = new Attempt(learningContentIds,
                    attemptJsonObject.getJsonNumber("totalScore").doubleValue(), attemptJsonObject.getString("attemptedOn")) ;
        }
        return new Progression(progressionJsonObject.getInt("totalAttempts"),
                               progressionJsonObject.getInt("successfulAttempts"), attempts);
    }

    private ConsolidatedReport getConsolidatedReportFromDataJson(JsonObject dataJsonObject, Progression progression) {
        return new ConsolidatedReport(dataJsonObject.getString("studentId"), dataJsonObject.getString("contextId"),
                                      dataJsonObject.getInt("reportType"), dataJsonObject.getInt("context"),
                                      dataJsonObject.getInt("learningContentType"), dataJsonObject.getInt("scope"),
                                      getHashMapFromJsonObject(dataJsonObject.getJsonObject("strengths")),
                                      getHashMapFromJsonObject(dataJsonObject.getJsonObject("weakness")),
                                      getHashMapFromJsonObject(dataJsonObject.getJsonObject("average")),
                                      getHashMapFromJsonObject(dataJsonObject.getJsonObject("high")),
                                      getHashMapFromJsonObject(dataJsonObject.getJsonObject("low")),
                                      getHashMapFromJsonObject(dataJsonObject.getJsonObject("myScore")),
                                      progression);
    }

    private HashMap<String, Integer> getHashMapFromJsonObject(JsonObject jsonObject) {
        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        if(jsonObject != null) {
            for (Map.Entry<String, JsonValue> entry : jsonObject.entrySet()) {
                hashMap.put(entry.getKey(), ((JsonNumber) entry.getValue()).intValue());
            }
        }
        return hashMap;
    }
}
