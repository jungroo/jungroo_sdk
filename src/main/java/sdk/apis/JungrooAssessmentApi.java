package sdk.apis;

import sdk.models.AssessmentQuestion;
import sdk.models.StudentChapterInfo;
import sdk.models.StudentAssessmentResponse;

import javax.json.JsonObject;
import java.io.IOException;

/**
 * Created by cibehariharan on 18/04/19.
 */
public class JungrooAssessmentApi extends JungrooApi {

    public JungrooAssessmentApi() {
        super("https://jungroo.com/");
    }

    public Long initializeAssessment(String studentId, String chapterId) throws IOException {
        StudentChapterInfo studentChapterInfo = new StudentChapterInfo(studentId, chapterId);
        JsonObject jsonObject = apiClient.postApi(hostEndPoint + "tce/initialize_assessment", getJsonString(studentChapterInfo));
        return jsonObject.getJsonNumber("data").longValueExact();
    }

    public AssessmentQuestion startAssessment(Long studentAssessmentId) throws IOException {
        JsonObject jsonObject = apiClient.postApi(hostEndPoint + "tce/start_assessment/" + studentAssessmentId, EMPTY_JSON);
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        AssessmentQuestion assessmentQuestion =
                new AssessmentQuestion(dataJsonObject.getString("learningObjectiveId"), dataJsonObject.getString("questionId"), dataJsonObject.getBoolean("isTerminated"));
        return assessmentQuestion;
    }

    public AssessmentQuestion resumeAssessment(Long studentAssessmentId) throws IOException {
        JsonObject jsonObject = apiClient.getApi(hostEndPoint + "tce/resume_assessment/" + studentAssessmentId);
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        AssessmentQuestion assessmentQuestion =
                new AssessmentQuestion(dataJsonObject.getString("learningObjectiveId"), dataJsonObject.getString("questionId"),dataJsonObject.getBoolean("isTerminated"));
        return assessmentQuestion;
    }

    public AssessmentQuestion continueAssessment(Long studentAssessmentId, int questionSequenceId, Double mark, Long timeTaken) throws IOException {
        StudentAssessmentResponse studentResponse = new StudentAssessmentResponse(questionSequenceId, mark, timeTaken);
        JsonObject jsonObject = apiClient.postApi(hostEndPoint + "tce/continue_assessment/" + studentAssessmentId, getJsonString(studentResponse));
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        AssessmentQuestion assessmentQuestion =
                new AssessmentQuestion(dataJsonObject.getString("learningObjectiveId"), dataJsonObject.getString("questionId"), dataJsonObject.getBoolean("isTerminated"));
        return assessmentQuestion;
    }
}
