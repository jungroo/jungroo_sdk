package sdk.apis;

import java.io.IOException;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.HttpMultipartMode;

import javax.json.Json;
import javax.json.*;
import javax.json.JsonReader;
import java.io.StringReader;
import java.io.File;

public class ApiClient {

    String secret, pricipalId;

    HttpClient client = new DefaultHttpClient();

    public ApiClient(String pricipalId, String secret) {
        this.secret = secret;
        this.pricipalId = pricipalId;
    }

    public JsonObject getApi(String requestUrl) throws IOException {
        HttpGet request = new HttpGet(requestUrl);
        request.addHeader("principal_id" , pricipalId);
        request.addHeader("secret" , secret);
        HttpResponse response = client.execute(request);
        if(response.getEntity() != null) {
            return getJsonObjectFromEntity(response.getEntity());
        } else {
            return null;
        }
    }

    public JsonObject putApi(String requestUrl, String jsonString) throws IOException {
        HttpPut request = new HttpPut(requestUrl);
        request.addHeader("principal_id" , pricipalId);
        request.addHeader("secret" , secret);
        HttpEntity requestEntity = new StringEntity(jsonString, ContentType.APPLICATION_JSON);
        request.setEntity(requestEntity);
        HttpResponse response = client.execute(request);
        if(response.getEntity() != null) {
            return getJsonObjectFromEntity(response.getEntity());
        } else {
            return null;
        }
    }

    public JsonObject postApi(String requestUrl, String jsonString) throws IOException {
        HttpPost request = new HttpPost(requestUrl);
        request.addHeader("principal_id" , pricipalId);
        request.addHeader("secret" , secret);
        HttpEntity requestEntity = new StringEntity(jsonString, ContentType.APPLICATION_JSON);
        request.setEntity(requestEntity);
        HttpResponse response = client.execute(request);
        if(response.getEntity() != null) {
            return getJsonObjectFromEntity(response.getEntity());
        } else {
            return null;
        }
    }

    public JsonObject putFileApi(String requestUrl, String fileName) throws IOException {
        HttpPut request = new HttpPut(requestUrl);
        request.addHeader("principal_id" , pricipalId);
        request.addHeader("secret" , secret);
        File file = new File(fileName);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addBinaryBody("file", file, ContentType.DEFAULT_BINARY, fileName);
        HttpEntity entity = builder.build();
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        if(response.getEntity() != null) {
            return getJsonObjectFromEntity(response.getEntity());
        } else {
            return null;
        }
    }


    private JsonObject getJsonObjectFromEntity(HttpEntity responseEntity) throws IOException {
        String responseBody = EntityUtils.toString(responseEntity);
        JsonReader jsonReader = Json.createReader(new StringReader(responseBody));
        JsonObject jsonObject = jsonReader.readObject();
        jsonReader.close();
        return jsonObject;
    }
}
