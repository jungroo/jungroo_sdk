package sdk.apis;

import sdk.models.StudentBook;

import javax.json.JsonObject;
import java.io.IOException;

/**
 * Created by cibehariharan on 23/04/19.
 */
public class JungrooSessionApi  extends JungrooApi {

    public JungrooSessionApi() {
        super("https://cms.jungroo.com/");
    }

    public Boolean initializeBookForStudent(String studentId, String BookId) throws IOException {
        StudentBook studentBook = new StudentBook(studentId, BookId);
        JsonObject jsonObject = apiClient.postApi(hostEndPoint + "tce/initialize_book_for_student", getJsonString(studentBook));
        return true;
    }
}
