package sdk.apis;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Created by cibehariharan on 11/04/19.
 */
public class JungrooApi {

    String pricipalId = "", secret = "";

    ApiClient apiClient = new ApiClient(pricipalId, secret);

    String hostEndPoint;

    String EMPTY_JSON = "{}";

    public JungrooApi(String hostEndPoint) {
        this.hostEndPoint = hostEndPoint;
    }

    String getJsonString(Object obj) throws  IOException {
        ObjectMapper mapperObj = new ObjectMapper();
        return mapperObj.writeValueAsString(obj);
    }
}
