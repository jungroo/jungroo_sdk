package sdk.apis;

import javax.json.JsonObject;
import java.io.IOException;

/**
 * Created by cibehariharan on 18/04/19.
 */
public class JungrooContentApi extends JungrooApi {
    public JungrooContentApi() {
        super("https://cms.jungroo.com/");
    }

    public Boolean uploadMappingFile(String fileName) throws IOException {
        JsonObject jsonObject = apiClient.putFileApi(hostEndPoint + "tce/upload_tp_lo_map", fileName);
        return jsonObject.getBoolean("data");
    }

    public Boolean uploadTeachingPointInfo(String fileName) throws IOException {
        JsonObject jsonObject = apiClient.putFileApi(hostEndPoint + "tce/upload_tp_info", fileName);
        return jsonObject.getBoolean("data");
    }

    public Boolean uploadResourceInfo(String fileName) throws IOException {
        JsonObject jsonObject = apiClient.putFileApi(hostEndPoint + "tce/upload_resource_info", fileName);
        return jsonObject.getBoolean("data");
    }

    public Boolean uploadQuestionResources(String fileName) throws IOException {
        JsonObject jsonObject = apiClient.putFileApi(hostEndPoint + "tce/upload_question_lo_map", fileName);
        return jsonObject.getBoolean("data");
    }

    public Boolean uploadConceptDependencies(String fileName) throws IOException {
        JsonObject jsonObject = apiClient.putFileApi(hostEndPoint + "tce/upload_concept_dependencies", fileName);
        return jsonObject.getBoolean("data");
    }

    public Boolean uploadBookXMLFile(String fileName) throws IOException {
        JsonObject jsonObject = apiClient.putFileApi(hostEndPoint + "/tce/store_book_info", fileName);
        return jsonObject.getBoolean("data");
    }
}
