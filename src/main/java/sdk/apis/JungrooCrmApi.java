package sdk.apis;

import sdk.models.ClassroomInfo;

import javax.json.JsonArray;
import javax.json.JsonObject;
import java.io.IOException;

public class JungrooCrmApi extends JungrooApi {
    public JungrooCrmApi() {
        super("https://jungroo.com/");
    }

    public void createClassroom(String classroomId, String schoolId,
                                int grade, String division,
                                String[] studentIds) throws IOException {
        ClassroomInfo classroomInfo = new ClassroomInfo(classroomId, schoolId, grade, division, studentIds);
        JsonObject jsonObject = apiClient.putApi(hostEndPoint + "/tce/create_classroom", getJsonString(classroomInfo));
    }

    public ClassroomInfo getClassroomDetails(String classroomId) throws IOException {
        JsonObject jsonObject = apiClient.getApi(hostEndPoint + "/classroom_info/" + classroomId);
        JsonObject dataJsonObject = jsonObject.getJsonObject("data");
        JsonArray studentIdsArray = dataJsonObject.getJsonArray("studentIds");
        String[] studentIds = new String[studentIdsArray.size()];
        for (int studentIndex = 0; studentIndex < studentIdsArray.size(); studentIndex++) {
            studentIds[studentIndex] = studentIdsArray.getString(studentIndex);
        }
        return new ClassroomInfo(dataJsonObject.getString("classroomId"), dataJsonObject.getString("schoolId"),
                                 dataJsonObject.getInt("grade"), dataJsonObject.getString("division"), studentIds);
    }
}